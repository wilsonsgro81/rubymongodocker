### Test Connect dB MongoDb

```bash
docker-compose up -d
```

```bash
docker-compose exec ruby bash
```

```bash
cd /usr/src/app
```

```bash
chmod 777 your-daemon-or-script.rb
```

```bash
./your-daemon-or-script.rb
```

```bash
Client Connection: 
#<Mongo::Cluster:0x47184869909160 servers=[#<Mongo::Server:0x47184869931460 address=mongo:27017>] topology=Single[mongo:27017]>

Collection Names: 
Connected!
```

#### Admin Mongo

	- user: root
	- password: example

>[admin mongo](http://127.0.0.1:8081/)

#### Teach

> [run-your-own-gem-server](https://guides.rubygems.org/run-your-own-gem-server/)

> [guides.rubygems.org/rubygems-basics](https://guides.rubygems.org/rubygems-basics/)

> [docs.objectrocket.com/mongodb_ruby_examples.html](https://docs.objectrocket.com/mongodb_ruby_examples.html)