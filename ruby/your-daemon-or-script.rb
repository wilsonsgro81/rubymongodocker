#!/usr/bin/env ruby
require 'mongo'

# Turn off debug-mode
Mongo::Logger.logger.level = Logger::WARN

client_host = ['mongo:27017']
client_options = {
  database: 'admin',
  user: 'root',
  password: 'example',
  ssl: false
}

begin
  client = Mongo::Client.new(client_host, client_options)
  puts('Client Connection: ')
  puts(client.cluster.inspect)
  puts
  puts('Collection Names: ')
  puts(client.database.collection_names)
  puts('Connected!')
  client.close
rescue StandardError => err
  puts('Error: ')
  puts(err)
end